import 'package:google_maps_flutter/google_maps_flutter.dart';

class RequestModel{
  int id;
  int userId;
  String userName;
  String sourceCity;
  String destinationCity;
  LatLng sourceLocation;
  LatLng destinationLocation;
  int status;
  String date;
  String time;

  RequestModel({required this.id,required this.userId,required this.userName,required this.sourceLocation,
    required this.destinationLocation,required this.status,required this.date,required this.time,required this.sourceCity,required this.destinationCity});

  static List<RequestModel> requests=[
    // RequestModel(id: 1, userId: 1, userName: "ali", sourceLocation: LatLng(222.3,23.44), destinationLocation: LatLng(222.3,23.44), status: 0, date: "22/2/2", time: "10:30"),
    // RequestModel(id: 2, userId: 1, userName: "ali", sourceLocation: LatLng(222.3,23.44), destinationLocation: LatLng(222.3,23.44), status: 1, date: "22/2/2", time: "10:30"),
    // RequestModel(id: 3, userId: 1, userName: "ali", sourceLocation: LatLng(222.3,23.44), destinationLocation: LatLng(222.3,23.44), status: 1, date: "22/2/2", time: "10:30"),
  ];
}