import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:taxi_app_graduation/controller/auth_controller/login_controller.dart';
import 'package:taxi_app_graduation/utils/app_colors.dart';
import 'package:taxi_app_graduation/utils/app_routes.dart';
import 'package:taxi_app_graduation/view/widgets/custom_button.dart';
import 'package:taxi_app_graduation/view/widgets/custom_text.dart';
import 'package:taxi_app_graduation/view/widgets/custom_text_field.dart';
class LoginScreen extends StatelessWidget {
  final GlobalKey<FormState> _formKey= GlobalKey<FormState>();
  LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Directionality(
        textDirection: TextDirection.rtl,
        child: Padding(
          padding: const EdgeInsets.only(
              top: 50,
              left: 20,
              right: 20
          ),
          child: GetBuilder<LoginController>(
            init: LoginController(remoteData: Get.find()),
            builder:(controller)=> ListView(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CustomText(text: 'مرحبا',fontSize: 30,),
                    GestureDetector(onTap: (){
                      Get.toNamed(AppRoutes.register);
                    },
                        child: CustomText(text: 'إنشاء حساب',fontSize: 18,color:AppColors.primaryColor,)),
                  ],
                ),
                Container(
                  width: double.infinity,
                  height: 250,
                  child: Image(
                    image: AssetImage('assets/images/logo.png'),
                  ),
                ),
                SizedBox(height: 20,),
                CustomTextField(text: 'رقم الهاتف',
                    hint: '09888',
                    inputType: TextInputType.phone,
                  controller: controller.phone,),
                SizedBox(height: 20,),
                CustomTextField(text: 'كلمة المرور',
                    hint: '*******',
                    isScure: true,
                  controller: controller.password,),
                SizedBox(height: 50,),
                CustomButton(text: 'تسجيل الدخول',onPress: (){
                  controller.login();
                }),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
